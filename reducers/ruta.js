function ruta(state=false, action){
    switch(action.type){
        case 'SET_RUTA':{
            return {...state, ...action.payload}
        }
        case 'REMOVE_RUTA':{
            return false
        }
        default :
            return state
    }
}

export default ruta;