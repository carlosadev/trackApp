import { combineReducers } from 'redux';
import navigation from './navigation';
import user from './user';
import ruta from './ruta';

const reducer = combineReducers({
     navigation,
     user,
     ruta,

})

export default reducer;