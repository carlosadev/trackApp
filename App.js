import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './store';
import Loading from './src/sections/components/loading';
import AppNavigatorWithState from './src/app-navigator-with-state';
//import BgTracking from './src/sections/containers/bgtracking';
import { Alert, View } from 'react-native';
import BackgroundGeolocation from 'react-native-mauron85-background-geolocation';
type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      region: null,
      locations: [],
      stationaries: [],
      isRunning: false
    };

   
  }
  componentDidMount() {

    BackgroundGeolocation.configure({
      desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
      stationaryRadius: 50,
      distanceFilter: 50,
      notificationTitle: 'Background tracking',
      notificationText: 'enabled',
      debug: false,
      startOnBoot: false,
      stopOnTerminate: false,
      locationProvider: BackgroundGeolocation.ACTIVITY_PROVIDER,
      interval: 30000,
      fastestInterval: 5000,
      activitiesInterval: 30000,
      stopOnStillActivity: false,
      httpHeaders: {
        'X-FOO': 'bar'
      },
      // customize post properties
      postTemplate: {
        lat: '@latitude',
        lon: '@longitude',
        foo: 'bar' // you can also add your own properties
      }
    });

     BackgroundGeolocation.on('location', location => {
     /*  console.log(store.getState().user.token)
      console.log('[DEBUG] BackgroundGeolocation location', location); */
       fetch('https://siscove.com:3001/location',
        {
          body: JSON.stringify({
            usuario: store.getState().user.token,
            latitud:location.latitude,
            longitud:location.longitude
          }),
          headers:{
           'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        method: 'POST',
        });
      BackgroundGeolocation.startTask(taskKey => {
        requestAnimationFrame(() => {
          const longitudeDelta = 0.01;
          const latitudeDelta = 0.01;
          const region = Object.assign({}, location, {
            latitudeDelta,
            longitudeDelta
          });
          const locations = this.state.locations.slice(0);
          locations.push(location);
          this.setState({ locations, region });
          BackgroundGeolocation.endTask(taskKey);
        });
      });
    });

    BackgroundGeolocation.on('error', (error) => {
      console.log('[ERROR] BackgroundGeolocation error:', error);
    });

     BackgroundGeolocation.on('foreground', () => {
      console.log('[INFO] App is in foreground');
    });

    BackgroundGeolocation.on('background', () => {
      console.log('[INFO] App is in background');
    });

    BackgroundGeolocation.checkStatus(({ isRunning }) => {
      this.setState({ isRunning });
    });
    
  }

     componentWillUnmount() {
      BackgroundGeolocation.events.forEach(event =>
        BackgroundGeolocation.removeAllListeners(event)
      );

  }

  render() {
    console.disableYellowBox = true;
    return (
      <Provider
        store={store}
      >
        
        <PersistGate
          loading={<Loading />}
          persistor={persistor}
        >
          <AppNavigatorWithState />
        </PersistGate>
      </Provider>
    );
  }
}
