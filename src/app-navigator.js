import React from 'react';
import {
  createStackNavigator,
  createSwitchNavigator,
  createDrawerNavigator
} from 'react-navigation';

import Login from './screens/containers/login';
import Home from './screens/containers/home';
import Perfil from './screens/containers/perfil';
import Ruta from './screens/containers/ruta';
import RutaVista from './screens/containers/rutaVista';
import RutaCrear from './screens/containers/rutaCrear';
import Loading from './screens/containers/loading';
import Header from './sections/components/header';
import DrawerComponent from './sections/components/drawer';


const Main = createStackNavigator(
  {
    Home: Home,
    RutaVista: RutaVista,
    RutaCrear: RutaCrear,
  },
  {
    navigationOptions: {
      header: Header,
    },
    cardStyle: {
      backgroundColor: 'white'
    }
  }
)

const DrawerNavigator = createDrawerNavigator(
  {
    Main: {
      screen: Main,
      navigationOptions: {
        title: 'Inicio',
      }
    },
    perfil:{
      screen:Perfil,
      navigationOptions: {
        title: 'Perfil',
      }
    },
    rutas:{
      screen:Ruta,
      navigationOptions:{
        title:'Ruta'
      },
    },
  },
  {
    drawerWidth: 200,
    drawerBackgroundColor: '#f6f6f6',
    contentComponent: DrawerComponent,
    contentOptions: {
      activeBackgroundColor: '#7aba2f',
      activeTintColor: 'white',
      inactiveTintColor: '#828282',
      inactiveBackgroundColor: 'white',
      itemStyle: {
        borderBottomWidth: .5,
        borderBottomColor: 'rgba(0,0,0,.5)'
      },
      labelStyle: {
        marginHorizontal: 5,
      },
      iconContainerStyle: {
        marginHorizontal: 5,
      }
    }
  }
)


const SwitchNavigator = createSwitchNavigator(
  {
    App: DrawerNavigator,
    Login: Login,
    Loading: Loading,
  },
  {
    initialRouteName: 'Loading',
  }
)



	

export default SwitchNavigator;