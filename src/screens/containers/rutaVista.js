import React, {Component} from 'react';
import {
    View,
    Text,
} from 'react-native';
import {connect} from 'react-redux';
class RutaVista extends Component{
    render(){
        return(
            <View>
                <Text>
                    aqui estamos viendo la ruta {this.props.ruta.idruta}
                </Text>
            </View>
        );
    }
}

function mapStateToProps(state){
    return {
        ruta: state.ruta
    }
}

export default connect(mapStateToProps)(RutaVista);