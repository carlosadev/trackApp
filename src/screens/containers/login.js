import React, { Component } from 'react';
import BackgroundGeolocation from 'react-native-mauron85-background-geolocation';
import {
  View,
  Text,
  TextInput,
  Button,
  StyleSheet,
  SafeAreaView,
  Image,
  TouchableOpacity,
  Alert
} from 'react-native';
import { connect } from 'react-redux';

class Login extends Component {
    constructor(props) {
    super(props);
    this.state = ({
    usuario:'',
    password:''
  });
  }
  

  handleLogin = () => {
    let datos = {};
    datos.usuario = this.state.usuario
    datos.pass = this.state.password
     fetch('https://siscove.com:3001/validationapp',
    {
      body: JSON.stringify({
        usuario:datos.usuario,
        password:datos.pass
      }),
      headers:{
       'Content-Type': 'application/json',
        'Accept': 'application/json'
    },
    method: 'POST',
    }).then(response => response.json())
    .then((response) => { 
      if(response.msg){
        this.props.dispatch({
          type: 'SET_USER',
          payload: {
            token: response.msg.iduser ,
            username: datos.usuario
          }
        });
         BackgroundGeolocation.start();
        this.props.navigation.navigate('Loading');
      }else{
        alert("Los datos no coinciden!!", "Los datos no coinciden!.")
      }
    });
  }

  handleUpdateInput = (texto,tipo) => {
    if(tipo == 'usuario'){
      this.setState({
        usuario:texto,
      })
    }else if(tipo == 'password'){
      this.setState({
        password:texto
      })
    }

  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View>
          <Image
            source={require('../../../assets/logo.png')}
            style={styles.logo}
          />
          <TextInput
            style={styles.input}
            placeholder="Nombre de usuario"
            placeholderTextColor="white"
            onChangeText={(text) => this.handleUpdateInput(text,'usuario')}
          />
          <TextInput
            style={styles.input}
            placeholder="Contraseña"
            placeholderTextColor="white"
            secureTextEntry={true}
            onChangeText={(text) => this.handleUpdateInput(text,'password')}
          />
          <TouchableOpacity
            onPress={this.handleLogin}
            style={styles.button}
          >
            <Text style={styles.buttonLabel}>Iniciar Sesión</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  logo: {
    width: 250,
    height: 80,
    resizeMode: 'contain',
    marginBottom: 20,
    
  },
  input: {
    marginBottom: 10,
    width: 250,
    height: 50,
    paddingHorizontal: 10,
    borderRadius: 5,
    backgroundColor: '#838383',
    color: 'white',
  },
  button: {
    backgroundColor: '#2b982b',
    borderRadius: 5,
  },
  buttonLabel: {
    color: 'white',
    padding: 10,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center'
  }
})

export default connect(null)(Login)