import React, { Component } from 'react';
import BackgroundGeolocation from 'react-native-mauron85-background-geolocation';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Button,
} from 'react-native';
import { connect } from 'react-redux';

class Profile extends Component {
  handleLogout =  () => {
    this.props.dispatch({
      type:'REMOVE_USER',

    })
    BackgroundGeolocation.stop();
    this.props.navigation.navigate('Loading');
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Text>{this.props.user.username}</Text>
        <Button
          title="Cerrar session"
          color="#67a52e"
          onPress={this.handleLogout}
        />
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }
})


function mapStateToProps(state){
  return{
    user: state.user
  }
}

export default connect(mapStateToProps)(Profile); 