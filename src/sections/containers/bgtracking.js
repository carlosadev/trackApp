import React, { Component } from 'react';
import { Alert, View } from 'react-native';
import { connect } from 'react-redux';
import BackgroundGeolocation from 'react-native-mauron85-background-geolocation';

class BgTracking extends Component {

 

  componentDidMount() {
 
    BackgroundGeolocation.on('location', location => {
      console.log(this.state.user)
      console.log('[DEBUG] BackgroundGeolocation location', location);
       fetch('http://192.168.1.3:3000/location',
        {
          body: JSON.stringify({
            latitud:location.latitude,
            longitud:location.longitude
          }),
          headers:{
           'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        method: 'POST',
        });
      BackgroundGeolocation.startTask(taskKey => {
        requestAnimationFrame(() => {
          const longitudeDelta = 0.01;
          const latitudeDelta = 0.01;
          const region = Object.assign({}, location, {
            latitudeDelta,
            longitudeDelta
          });
          const locations = this.state.locations.slice(0);
          locations.push(location);
          this.setState({ locations, region });
          BackgroundGeolocation.endTask(taskKey);
        });
      });
    });

    BackgroundGeolocation.on('error', (error) => {
      console.log('[ERROR] BackgroundGeolocation error:', error);
    });

     BackgroundGeolocation.on('foreground', () => {
      console.log('[INFO] App is in foreground');
    });

    BackgroundGeolocation.on('background', () => {
      console.log('[INFO] App is in background');
    });

    BackgroundGeolocation.checkStatus(({ isRunning }) => {
      this.setState({ isRunning });
    });

  }

     componentWillUnmount() {
      BackgroundGeolocation.events.forEach(event =>
        BackgroundGeolocation.removeAllListeners(event)
      );

  }

  render(){
    return(
      <View> </View>
    );
  }
}

export default connect(null)(BgTracking);